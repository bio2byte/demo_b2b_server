# README #

### About bio2Byte ###
Proteins are the molecular machines that make cells work. They perform a wide variety of functions through interactions with each other and many additional molecules. Traditionally, proteins are described in a single static state (a picture). It is now increasingly recognised that many proteins can adopt multiple states and move between these conformational states dynamically (a movie).

We investigate how the dynamics, conformational states and available experimental data of proteins relates to their amino acid sequence. Underlying physical and chemical principles are computationally unravelled through data integration, analysis and machine learning, so connecting them to biological events and improving our understanding of the way proteins work.

### About this repo ###

This is repository contains the code to illustrate the API functionalities of bio2Byte tools. It is made to be ran in [mybinder.org](https://mybinder.org/), although it should run locally. 
For the full tool, documentation, visualization and further documentation, please visit [our website](https://www.bio2byte.be/b2btools/).

### What is this repository for? ###

* Obtain biophysical properties predictions of proteins from their amino-acid sequence with our tools [1]
* Demo version 1.0.0.
* Programmatic access to bio2Byte tools via API.


### API submission limitation

There are limits of proteins that can be submitted at once in our tools. These limits depend on the tool and are listed bellow:

* MSAtools: 200
* AgMata: 10
* DynaMine, EFoldMine and DisoMine: 50

Please contact us for larger jobs, we'll be happy to assist.


### How do I get set up? ###

* For use in [mybinder.org](https://mybinder.org/) [2]:
    * Most likely, you arrived here with a link. You are set! Just click on the [jupyter file](https://bitbucket.org/bio2byte/demo_b2b_server/src/main/bio2Byte_api.ipynb) and start the demo. 
    * If you do not have a link to a binder, it is simpler to run locally or use other service (*eg*. [google colab](https://research.google.com/colaboratory/)) to run it on the cloud. 
    

* For local use:
    * Clone this repository.
    * Ensure the requirements in requirements.txt are installed.
    * Execute the jupyter notebook.
    

This notebook has been tested locally with Python 3.8 in a conda environment.


### Who do I talk to? ###

* Creator of this notebook: Jose Gavaldá-García ([jose.gavalda.garcia@vub.be](mailto:jose.gavalda.garcia@vub.be?Subject=b2b_server%20demo%20notebook))
* Server's admin: Adrian Díaz ([adrian.diaz@vub.be](mailto:adrian.diaz@vub.be?Subject=b2b_server%20demo%20notebook))
* Server's collaborator: Luciano Porto Kagami ([luciano.kagami@ufrgs.br](mailto:luciano.kagami@ufrgs.br?Subject=b2b_server%20demo%20notebook))
* Group leader: Wim F. Vranken ([wim.vranken@vub.be](mailto:wim.vranken@vub.be?Subject=b2b_server%20demo%20notebook))


### References ###
1. Kagami, L. P., Orlando, G., Raimondi, D., Ancien, F., Dixit, B., Gavaldá-García, J., Ramasamy, P., Roca-Martínez, J., Tzavella, K., & Vranken, W. (2021). b2bTools: online predictions for protein biophysical features and their conservation. Nucleic Acids Research, 1. https://doi.org/10.1093/nar/gkab425
2. Jupyter, P., Bussonnier, M., Forde, J., Freeman, J., Granger, B., Head, T., Holdgraf, C., Kelley, K., Nalvarte, G., Osheroff, A., Pacer, M., Panda, Y., Perez, F., Ragan-Kelley, B., & Willing, C. (2018). Binder 2.0 - Reproducible, interactive, sharable environments for science at scale. In F. Akici, D. Lippa, D. Niederhut, & M. Pacer (Eds.), {P}roceedings of the 17th {P}ython in {S}cience {C}onference (pp. 113–120). https://doi.org/10.25080/Majora-4af1f417-011

## How to cite us ##
For the tools used in this demo:
  - Kagami, L. P., Orlando, G., Raimondi, D., Ancien, F., Dixit, B., Gavaldá-García, J., Ramasamy, P., Roca-Martínez, J., Tzavella, K., & Vranken, W. (2021). b2bTools: online predictions for protein biophysical features and their conservation. Nucleic Acids Research, 1. https://doi.org/10.1093/nar/gkab425

For this demonstration: 
  - Refer to our zenodo entry for information on the citation: [https://doi.org/10.5281/zenodo.5040390](https://doi.org/10.5281/zenodo.5040390)